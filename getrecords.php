<?php

	include('koneksi.php');

	$sql = "SELECT * FROM records";

	$result = $conn->query($sql);

	$num_results = $result->num_rows;

	//cek jika data tidak 0
	if( $num_results > 0){ 

		$array = array();

		while( $row = $result->fetch_assoc() ){
			extract($row);
			$array[] = $row;
		}

		echo json_encode($array);

	}else{

		echo "Data Kosong";

	}
?>